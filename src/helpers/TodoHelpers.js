export default function addToList(prevList, newItem) {
    return [...prevList, newItem];
}
