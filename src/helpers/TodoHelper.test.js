import addToList from './TodoHelpers';

test('adds a todo item to list', () => {
    const prevList = ["Buy a notebook", "Edit the repository"]
    const newItem = "Prepare the camp bag"
    const newList = addToList(prevList, newItem);

    expect(newList).toHaveLength(3);
    expect(newList).toContain(newItem);
});