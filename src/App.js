import { useState, useRef, useEffect } from "react";
import TodoList from "./components/TodoList";
import addToList from "./helpers/TodoHelpers";
import {createTodo, getTodos} from './services/TodoService';
import './App.css';

const App = () => {

  const [toDoList, setToDoList] = useState([]);
  const todoInput = useRef();

  useEffect(() => {
    getTodos()
      .then(response => {
        setToDoList(response.data)
      }).catch((err) => {
        throw new Error(err.message);
      });
  }, []);

  const handleAddToDo = () => {
    const todoItem = todoInput.current.value;
    createTodo(todoItem)
      .then(response => {
        const newToDoList = addToList(toDoList, response.data)
        setToDoList(newToDoList);
      }).catch((err) => {
        throw new Error(err.message);
      });
  }

  return (<>
    <h1 id="page-title">To Do List</h1>
    <TodoList items={toDoList} />
    <div id="input-form-container">
      <input type="text" ref={todoInput}/>
      <button data-testid="add-btn" id="add-btn" onClick={handleAddToDo}>Add</button>
    </div>
  </>);
}

export default App;
