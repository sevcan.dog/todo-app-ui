import { render, screen } from '@testing-library/react';
import App from './App';

test('renders subcomponents', () => {
  render(<App />);

  const heading = screen.getByText(/To Do List/i);
  const toDoList = screen.getByRole('list');
  const toDoInput = screen.getByRole('textbox');
  const button = screen.getByTestId('add-btn');


  expect(heading).toBeInTheDocument();
  expect(toDoList).toBeInTheDocument();
  expect(toDoInput).toBeInTheDocument();
  expect(button).toHaveTextContent(/Add/i);
  expect(button).toBeInTheDocument();
});
