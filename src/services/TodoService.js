import axios from 'axios';

const API_URL = process.env.REACT_APP_URL;

export async function createTodo(description, baseURL) {
	let requestConfig = {
		baseURL: baseURL || API_URL, 
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json; charset=utf-8'
		}
	};
	return axios.post('/todo', {description: description}, requestConfig);
}

export async function getTodos(baseURL) {
	return axios.get('/todos', { baseURL: baseURL || API_URL })
}
