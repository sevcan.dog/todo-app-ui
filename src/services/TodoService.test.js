import { createTodo, getTodos } from './TodoService';
import { pactWith } from 'jest-pact';
import { eachLike, integer, string } from '@pact-foundation/pact/src/dsl/matchers';

require('dotenv').config();

const TODO = { description: 'Buy groceries' };
const POST_TODO_REQUEST = {
    method: 'POST',
    path: '/todo',
    body: TODO,
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json; charset=utf-8'
    }
}
const POST_TODO_RESPONSE = {
    status: 200,
    body: {id: integer(5)},
    headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json; charset=utf-8'
    },
}

const GET_TODOS_REQUEST = {
    method: 'GET',
    path: '/todos',
}
const GET_TODOS_RESPONSE = {
    status: 200,
    body: eachLike({id: 5, description: "Buy smt"}),
    headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json; charset=utf-8'
    },
}

// const DELETE_TODOS_REQUEST = {
//     method: 'DELETE',
//     path: '/todos',
// }
// const DELETE_TODOS_RESPONSE = {
//     status: 200,
//     body: ([]),
//     headers: {
//         'Access-Control-Allow-Origin': '*',
//         'Content-Type': 'application/json; charset=utf-8'
//     }
// }
 
pactWith({ consumer: 'todo-app-ui', provider: 'todo-app-api' }, provider => {

    describe('post endpoint', () => {

        beforeEach(() => 
            provider.addInteraction({
                uponReceiving: 'A request for adding a todo',
                withRequest: POST_TODO_REQUEST,
                willRespondWith: POST_TODO_RESPONSE
            }))


        test('sends a request to add a todo', async () => {
            await createTodo(TODO.description, provider.mockService.baseUrl)
                .then((response) => 
                    expect(response.data).toHaveProperty('id', 5)
                )
        });
    });

    describe('get endpoint', () => {

        beforeEach(() => 
            provider.addInteraction({
                uponReceiving: 'A request for getting todos',
                withRequest: GET_TODOS_REQUEST,
                willRespondWith: GET_TODOS_RESPONSE
            }))
    
    
        test('sends a request to get todos', async () => {
                await getTodos(provider.mockService.baseUrl)
                    .then((response) => 
                        expect(response.data[0]).toHaveProperty('id', 5)
                    )
            });
        });    
});
