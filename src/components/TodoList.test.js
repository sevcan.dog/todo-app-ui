import { render, screen } from '@testing-library/react';
import TodoList from './TodoList';

test('initially renders empty todo list', () => {
    render(<TodoList />)
    const toDoList = screen.getByRole("list");
    expect(toDoList).not.toHaveTextContent();
});

test('renders a list', () => {
    const todos = [{id: 1, description:"Buy a notebook"}, {id:2, description: "Edit the repository"}];
    render(<TodoList items={todos}/>);

    const toDoList = screen.getByRole("list");
    todos.forEach(todo => expect(toDoList).toHaveTextContent(todo.description));
});