# Todo App UI

This project uses:
* [axios](https://www.npmjs.com/package/axios) for HTTP requests
* [testing-library](https://testing-library.com/) for testing
* [TAIKO](https://taiko.dev/) for the acceptance test
* [Pact.io](https://www.npmjs.com/package/@pact-foundation/pact) for the consumer driven contract (CDC) testing
* [jest-pact](https://github.com/Selvatico/go-mocket) for writing Pact files easily with Jest
* [dotenv](https://www.npmjs.com/package/dotenv) for manipulating environment variables

```
    .
    ├── public
    │   └── ...
    ├── src  
    │   ├── ...
    │   └── services
    │         ├── TodoService
    │         └── TodoService.test --> includes CDC test
    ├── .dockerignore   
    ├── .env.development  
    ├── .env.production  
    ├── .gitignore  
    ├── .gitlab-ci.yml --> includes pipeline config  
    ├── acceptanceTest.js --> includes the acceptance test
    ├── DockerFile   
    ├── nginx.conf --> includes nginx config for static serving  
    ├── package-lock.json 
    ├── package.json
    ├── publish.js --> includes script to publish a Pact contract
    ├── README.md  
    ├── ui-deployment.yaml  
    └── ui-service.yaml
```


## Usage

### With Docker:

→ **Requirements**
* Docker

1. Run `docker build --build-arg URL=_YOUR_API_URL_ --build-arg BROKER_URL=_YOUR_BROKER_URL_ --build-arg BROKER_TOKEN=_YOUR_BROKER_TOKEN_ -t todo-app-ui .`
2. Run `docker run -dp 3000:3000 todo-app-ui`
3. Route to [localhost:3000/](http://localhost:3000/)

### Without Docker:

→ **Requirements**
* Node.js (developed with v14.15.3)

#### Test:

1. Create .env.test.local file and store 
```
REACT_APP_PACT_BROKER_BASE_URL=_YOUR_PACT_BROKER_BASE_URL_
REACT_APP_PACT_BROKER_TOKEN=_YOUR_PACT_BROKER_TOKEN_

REACT_APP_URL=_YOUR_API_TEST_URL_
REACT_APP_UI_URL=_YOUR_UI_TEST_URL_
```
2. Run `npx taiko acceptanceTest.js ` to run the acceptance test
2. Run `npm test` to run all tests
3. Run `npm run test:publish` to publish Pact contract to the Pack broker
### Develop:
- Run `npm start` to develop

### Build:
1. Configure .env.production and run `npm run build` to build
2. Run `serve -s build` to see locally

### To use Gitlab CI:

- Create the following variables in PROTECTED mode

| Name            | Value                    | Environment |
|-----------------|--------------------------|-------------|
| BROKER_TOKEN    | _YOUR_PACT_BROKER_TOKEN_ | All         |
| BROKER_URL      | _YOUR_PACT_BROKER_URL_   | All         |
| SERVICE_ACCOUNT | _YOUR_GCLOUD_SA_KEY_     | test        |
| URL             | _YOUR_TEST_API_URL_      | test        |
