const { openBrowser, goto, textBox, focus, write, click, closeBrowser } = require('taiko');

(async () => {
    try {
        await openBrowser();
        await goto(process.env.REACT_APP_UI_URL);
        isTitleExists = await text(/To do List/i).exists()
        if(!isTitleExists){
            process.exit(1);
        }; 
        await focus(textBox());
        await write("Call the mother");
        await click("Add");
        islistItemExists = await listItem("Call the mother").exists()
        if(!islistItemExists){
            process.exit(1);
        };  
    } catch (error) {
        console.error(error);
    } finally {
        await closeBrowser();
    }

    // try {

    //     /*
    //     Given I have multiple items on todo list
    //     When I click Delete All button 
    //     Then I want to see an empty list
    //      */
    //     await openBrowser();
    //     await goto(process.env.REACT_APP_UI_URL);
    //     await focus(textBox());
    //     await write("Asdf");
    //     await click("Add");
    //     await focus(textBox());
    //     await write("Hello");
    //     await click("Add");

    //     await click("Delete All");
    //     const isFirstItemExists = await listItem("Asdf").exists()
    //     const isSecondItemExists = await listItem("Hello").exists()

    //     if(isFirstItemExists || isSecondItemExists) {
    //         process.exit(2);
    //     }
    // } catch (error) {
    //     console.error(error);
    // } finally {
    //     await closeBrowser();
    // }
})();
