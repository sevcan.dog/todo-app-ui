require('dotenv').config({path: './.env.test.local'})
const { Publisher } = require("@pact-foundation/pact")
const path = require("path")
const childProcess = require("child_process")

const exec = command =>
  childProcess
    .execSync(command)
    .toString()
    .trim()

const opts = {
  pactFilesOrDirs: [path.resolve(process.cwd(), "pact/pacts")],
  pactBroker: process.env.REACT_APP_PACT_BROKER_BASE_URL,
  pactBrokerToken: process.env.REACT_APP_PACT_BROKER_TOKEN,
  consumerVersion: new Date().toISOString(),
}

new Publisher(opts)
  .publishPacts()
  .then(() => {
    console.log("Pact contract publishing complete!")
    console.log("")
    console.log(`Head over to ${process.env.REACT_APP_PACT_BROKER_BASE_URL} to see your published contracts.`)
  })
  .catch(e => {
    console.log("Pact contract publishing failed: ", e.message)
  })