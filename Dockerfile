# Bundle static assets with nginx
FROM node:alpine as builder

ENV NODE_ENV production

# Set working directory
WORKDIR /app

# install node modules and build assets
COPY . ./

ARG URL
ARG BROKER_URL
ARG BROKER_TOKEN
RUN echo $URL $BROKER_URL $BROKER_TOKEN

ENV REACT_APP_PACT_BROKER_BASE_URL $BROKER_URL
ENV REACT_APP_PACT_BROKER_TOKEN $BROKER_TOKEN
ENV REACT_APP_URL $URL
ENV NODE_OPTIONS=--openssl-legacy-provider

RUN npm i
RUN npm run build

# Bundle static assets with nginx
FROM nginx:1.21.0-alpine

# Copy built assets from builder
COPY --from=builder /app/build /usr/share/nginx/html

# Add your nginx.conf
COPY nginx.conf /etc/nginx/conf.d/default.conf

# Expose port
EXPOSE 3000

# Start nginx
CMD ["nginx", "-g", "daemon off;"]
